package main

import (
	"discordBot/helpers"
	"discordBot/monitors"
	"github.com/bwmarrin/discordgo"
	"log"
)

func main() {
	log.SetFlags(log.Lshortfile)
	//dg, err := discordgo.New("Bot " + os.Getenv("TOKEN"))
	//if err != nil {
	//	fmt.Println("error creating Discord session,", err)
	//	return
	//}
	//dg.AddHandler(messageCreate)
	//
	//err = dg.Open()
	//if err != nil {
	//	fmt.Println("error opening connection,", err)
	//	return
	//}
	//
	//fmt.Println("Bot is now running.  Press CTRL-C to exit.")
	//go monitors.NewCraigsListWatcher("464160029961093130")
	//sc := make(chan os.Signal, 1)
	//signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	//<-sc
	//
	//dg.Close()
	err := monitors.ParseRSS()
	if err != nil {
		log.Println(err)
	}
}

func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {

	if m.Author.ID == s.State.User.ID {
		return
	}

	helpers.HandleCommands(s, m)

}
