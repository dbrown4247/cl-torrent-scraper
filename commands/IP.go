package commands

import (
	"fmt"
	"github.com/GlenDC/go-external-ip"
	"github.com/bwmarrin/discordgo"
)

func getOutboundIP() string {
	consensus := externalip.DefaultConsensus(nil, nil)
	ip, err := consensus.ExternalIP()
	if err != nil {
		fmt.Println(err)
		return "Error getting IP"
	}
	return ip.String()
}

func IP(s *discordgo.Session, m *discordgo.MessageCreate) {
	_, _ = s.ChannelMessageSend(m.ChannelID, fmt.Sprintf("External IP: %s", getOutboundIP()))
}
