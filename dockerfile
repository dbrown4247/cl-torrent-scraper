FROM golang:1.14-alpine AS builder
ENV GOMODULE111=ON
WORKDIR $GOPATH/src/discordBot
RUN echo $GOPATH
RUN apk add --update --no-cache ca-certificates bash
COPY go.mod .
COPY go.sum .
RUN go mod download

COPY . .
RUN go get -d -v
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -o /go/bin/goapp
FROM alpine:latest AS discordbot
RUN apk add --update --no-cache ca-certificates
COPY --from=builder /go/bin/goapp /go/bin/goapp
WORKDIR $GOPATH/src/discordBot
COPY . .
ENTRYPOINT ["/go/bin/goapp"]