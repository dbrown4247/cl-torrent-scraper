package util

import (
	"bytes"
	"context"
	"discordBot/models"
	"encoding/json"
	"fmt"
	"github.com/elastic/go-elasticsearch/v7"
	"github.com/elastic/go-elasticsearch/v7/esapi"
	"github.com/kenshaw/imdb"
	"hash/fnv"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"time"
)

var torrentCache map[string]bool
var es *elasticsearch.Client

func init() {
	cfg := elasticsearch.Config{
		Addresses: []string{
			os.Getenv("ELASTIC_SEARCH_HOST"),
		},
		Username: os.Getenv("ELASTIC_SEARCH_USER"),
		Password: os.Getenv("ELASTIC_SEARCH_PASS"),
	}
	var err error
	es, err = elasticsearch.NewClient(cfg)
	if err != nil {
		log.Println(err)
		panic(err)
	}
}

func UpsertMovie(result *imdb.MovieResult) error {
	t, err := time.Parse("02 Jan 2006", result.Released)
	if err != nil {
		result.Released = time.Time{}.Format(time.RFC3339)
	} else {
		result.Released = t.Format(time.RFC3339)
	}
	rating, err := strconv.ParseFloat(result.ImdbRating, 64)
	if err != nil {
		result.ImdbRating = fmt.Sprint(rating)
	} else {
		result.ImdbRating = "-1"
	}
	byt, err := json.Marshal(result)
	if err != nil {
		log.Println(err)
		return err
	}
	req := esapi.IndexRequest{
		Index:        "torrentscraper",
		DocumentType: "_doc",
		DocumentID:   fmt.Sprint(hash(result.ImdbID)),
		Body:         bytes.NewReader(byt),
	}
	res, err := req.Do(context.Background(), es)
	defer res.Body.Close()
	if err != nil {
		log.Println(err)
		return err
	}
	byt, _ = ioutil.ReadAll(res.Body)
	log.Println(string(byt))
	return nil
}

func InsertTorrent(torrent *models.Torrent) error {
	if _, ok := torrentCache[torrent.Link]; !ok {
		byt, err := json.Marshal(torrent)
		if err != nil {
			log.Println(err)
			return err
		}
		req := esapi.IndexRequest{
			Index:        "torrentscraper",
			DocumentType: "_doc",
			DocumentID:   fmt.Sprint(hash(torrent.Link)),
			Body:         bytes.NewReader(byt),
		}
		res, err := req.Do(context.Background(), es)
		defer res.Body.Close()
		if err != nil {
			log.Println(err)
			return err
		}
	}
	return nil
}

func hash(s string) uint32 {
	h := fnv.New32a()
	h.Write([]byte(s))
	return h.Sum32()
}
