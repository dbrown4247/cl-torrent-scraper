package util

import (
	"discordBot/models"
	"fmt"
	"github.com/kenshaw/imdb"
	"log"
	"os"
	"time"
)

var omdb *imdb.Client
var omdbCache = make(map[string]*imdb.MovieResult)
var failedTitles = make(map[string]bool)
var lastCacheClear = time.Now()

func init() {
	omdb = imdb.New(os.Getenv("OMDBAPIKEY"))
}

func GetMovieByTitle(title string, year string) (*imdb.MovieResult, bool, error) {
	if failedTitles[fmt.Sprint(title, year)] {
		return nil, false, models.MovieAlreadyFailed
	}
	if lastCacheClear.Add(24 * time.Hour).After(time.Now()) {
		lastCacheClear = time.Now()
		omdbCache = make(map[string]*imdb.MovieResult)
	}
	if res, ok := omdbCache[fmt.Sprint(title, year)]; ok {
		return res, true, nil
	}
	res, err := omdb.MovieByTitle(title, year)
	if err != nil {
		failedTitles[fmt.Sprint(title, year)] = true
		log.Printf("%v [%s - %s]", err, title, year)
		return nil, false, err
	}
	omdbCache[fmt.Sprint(title, year)] = res
	return res, false, nil
}
