package monitors

import (
	"discordBot/models"
	"discordBot/util"
	"encoding/xml"
	"fmt"
	"golang.org/x/net/html/charset"
	"log"
	"net/http"
	"os"
	"regexp"
	"strings"
)

var titlePattern = regexp.MustCompile("(?P<title>[\\w ]+)(?:(?P<year>(?:20|19)\\d{2})|(?P<season>S\\d{1,2}(?:E\\d{1,2})?))")

func ParseRSS() error {
	tlrss, err := getTLRSS()
	if err != nil {
		log.Println(err)
		return err
	}
	for _, v := range tlrss.Channel.Torrents {
		torrentTitle, err := getTorrentFields(v.Title)
		if err != nil {
			continue
		}
		omdbData, cached, err := util.GetMovieByTitle(torrentTitle.Title, torrentTitle.Year)
		if err != nil {
			log.Println(err)
			continue
		}
		if !cached {
			err := util.UpsertMovie(omdbData)
			if err != nil {
				log.Println(err)
				return err
			}
		}
		v.ImdbID = omdbData.ImdbID
		v.Season = torrentTitle.Season
		err = util.InsertTorrent(&v)
		if err != nil {
			log.Println(err)
			return err
		}
	}

	return nil
}

func getTorrentFields(title string) (*models.TorrentTitle, error) {
	match := titlePattern.FindStringSubmatch(title)
	result := make(map[string]string)
	for i, name := range titlePattern.SubexpNames() {
		if i > len(match) {
			break
		}
		if i != 0 && name != "" {
			result[name] = match[i]
		}
	}

	if len(result["title"]) == 0 {
		log.Printf("%v: [%s]\n", models.MovieTitleNotFoundError, title)
		return nil, models.MovieTitleNotFoundError
	}

	return &models.TorrentTitle{
		Title:  strings.TrimSpace(result["title"]),
		Year:   strings.TrimSpace(result["year"]),
		Season: strings.TrimSpace(result["season"]),
	}, nil
}

func getTLRSS() (*models.TLrss, error) {

	client := &http.Client{}
	req, err := http.NewRequest("GET", os.Getenv("TLRSS"), nil)
	if err != nil {
		fmt.Println(err)
	}
	resp, err := client.Do(req)
	defer resp.Body.Close()
	if err != nil {
		log.Println(err)
		return nil, err
	}
	decoder := xml.NewDecoder(resp.Body)
	decoder.CharsetReader = charset.NewReaderLabel
	rss := &models.TLrss{}
	err = decoder.Decode(rss)
	if err != nil {
		log.Print(err)
		return nil, err
	}
	return rss, nil
}
