package helpers

import (
	"discordBot/commands"
	"github.com/bwmarrin/discordgo"
	"strings"
)

var commandMap = make(map[string]func(s *discordgo.Session, m *discordgo.MessageCreate))

func init() {
	commandMap["!ip"] = commands.IP
}

func HandleCommands(s *discordgo.Session, m *discordgo.MessageCreate) {
	if v, ok := commandMap[trimAt(m.Content, " ")]; ok {
		v(s, m)
	}
}

func trimAt(input string, substr string) string {
	index := strings.Index(input, substr)

	if index > 0 {
		return input[1:index]
	}
	return input
}
