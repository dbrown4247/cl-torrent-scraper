package models

import (
	"encoding/xml"
	"errors"
)

var MovieTitleNotFoundError = errors.New("error parsing movie title from torrent title")
var MovieAlreadyFailed = errors.New("movie look up failed previously")

type Torrent struct {
	ImdbID      string
	Season      string
	Text        string `xml:",chardata"`
	Title       string `xml:"title"`
	PubDate     string `xml:"pubDate"`
	Category    string `xml:"category"`
	Guid        string `xml:"guid"`
	Comments    string `xml:"comments"`
	Link        string `xml:"link"`
	Description string `xml:"description"`
}

type TorrentTitle struct {
	Title  string
	Year   string
	Season string
}

type TLrss struct {
	XMLName xml.Name `xml:"rss"`
	Text    string   `xml:",chardata"`
	Version string   `xml:"version,attr"`
	Atom    string   `xml:"atom,attr"`
	Channel struct {
		Text  string `xml:",chardata"`
		Title string `xml:"title"`
		Link  struct {
			Text string `xml:",chardata"`
			Href string `xml:"href,attr"`
			Rel  string `xml:"rel,attr"`
			Type string `xml:"type,attr"`
		} `xml:"link"`
		Description string    `xml:"description"`
		Language    string    `xml:"language"`
		Ttl         string    `xml:"ttl"`
		Torrents    []Torrent `xml:"item"`
	} `xml:"channel"`
}
